**README**


INSTALLATION PROCESS

1. Make sure you have installed JDK (In submission I was using JDK 1.8.0_151)

2. Please open your IDE and try to get source using GIT

3. Link to the source: https://bitbucket.org/arkadio-rudald/toy-robot-simulator

Personally I recommend using IntelliJ because it natively supports Gradle.

For IntelliJ:

1. Check out from the VCS. When wizard asks about choosing Gradle distribution I recommend using Gradle Wrapper in this case.

2. Find the terminal in Intellij and type:

  a) gradlew build - to build solution,

  b) gradlew test - to execute the tests
  
3. After running a Program you can use one of following commands in standard input

  a) PLACE x,y,f - x,y are coordinates, f is facing direction
  b) MOVE
  c) LEFT
  d) RIGHT
  e) REPORT
  f) STOP - to end program

Solution was created and tested using IntelliJ 2017.3.2 IDE and Gradle Project

For Eclipse:

1. If it does not support Gradle, please download it from Eclipse Marketplace

2. Check out from the VCS. Import as an existing Gradle project, When wizard asks about choosing Gradle distribution I recommend using Gradle Wrapper in this case.

3. In Eclipse go to Window -> Show View -> Other -> Gradle -> Gradle Tasks

4. Click on the task you want to execute (Build, Test)

5. After running a Program you can use one of following commands in standard input

  a) PLACE x,y,f - x,y are coordinates, f is facing direction
  b) MOVE
  c) LEFT
  d) RIGHT
  e) REPORT
  f) STOP - to end program

Solution was tested using Eclipse Oxygen 2 and Gradle Project