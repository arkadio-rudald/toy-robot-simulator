package com.arkadiusz.toysimulator.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobotTest {
    private Robot robot;

    @BeforeEach
    void setUp() {
        robot = new Robot(FacingDirection.NORTH, new Position(0,0));
    }

    @Test
    void move() {
        robot.move();
        assertEquals(new Position(0,1), robot.getCurrentPosition());
    }

    @Test
    void turnRight() {
        robot.turnRight();
        assertEquals(FacingDirection.EAST, robot.getFacingDirection());
    }

    @Test
    void turnLeft() {
        robot.turnLeft();
        assertEquals(FacingDirection.WEST, robot.getFacingDirection());
    }

    @Test
    void report() {
        String report = robot.report();
        assertEquals("0,0,NORTH", report);
    }

    @Test
    void turnRight3times()
    {
        robot.turnRight();
        robot.turnRight();
        robot.turnRight();
        assertEquals(FacingDirection.WEST, robot.getFacingDirection());
    }

    @Test
    void turnLeft3times()
    {
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
        assertEquals(FacingDirection.EAST, robot.getFacingDirection());
    }

    @Test
    void turnRight4times()
    {
        robot.turnRight();
        robot.turnRight();
        robot.turnRight();
        robot.turnRight();
        assertEquals(FacingDirection.NORTH, robot.getFacingDirection());
    }

    @Test
    void turnLeft4times()
    {
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
        robot.turnLeft();
        assertEquals(FacingDirection.NORTH, robot.getFacingDirection());
    }

    @Test
    void moveTurnRightMove()
    {
        robot.move();
        robot.turnRight();
        robot.move();
        assertEquals(FacingDirection.EAST, robot.getFacingDirection());
        assertEquals(new Position(1,1), robot.getCurrentPosition());
    }
}