package com.arkadiusz.toysimulator;

import com.arkadiusz.toysimulator.model.FacingDirection;
import com.arkadiusz.toysimulator.model.Position;
import com.arkadiusz.toysimulator.model.Robot;
import com.arkadiusz.toysimulator.model.Table;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
Integration testing of entire Robot Simulator system.
 */
class ToyRobotSimulatorIntegrationTest {
    private RobotController robotController;

    @BeforeEach
    void setUp() {
        robotController = new RobotController(new ActionFactory(new Table(0,0,5,5)));
    }

    @Test
    void createRobot() {
        robotController.performTask("PLACE 1,1,NORTH");
        assertEquals(new Robot(FacingDirection.NORTH, new Position(1,1)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 0,0,EAST");
        assertEquals(new Robot(FacingDirection.EAST, new Position(0,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,5,SOUTH");
        assertEquals(new Robot(FacingDirection.SOUTH, new Position(5,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,5,WEST");
        assertEquals(new Robot(FacingDirection.WEST, new Position(5,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE -1,0,SOUTH");
        assertEquals(null, robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,-1,SOUTH");
        assertEquals(null, robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,6,SOUTH");
        assertEquals(null, robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 6,5,SOUTH");
        assertEquals(null, robotController.getActionFactory().getRobot());
    }

    @Test
    void moveRobotHorizontally()
    {
        robotController.performTask("PLACE 0,0,EAST");

        for (int i = 0; i < 5; i++)
            robotController.performTask("MOVE");

        assertEquals(new Robot(FacingDirection.EAST, new Position(5,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("LEFT");
        robotController.performTask("LEFT");

        for (int i = 0; i < 5; i++)
            robotController.performTask("MOVE");

        assertEquals(new Robot(FacingDirection.WEST, new Position(0,0)), robotController.getActionFactory().getRobot());
    }

    @Test
    void moveRobotVertically()
    {
        robotController.performTask("PLACE 0,0,NORTH");

        for (int i = 0; i < 5; i++)
            robotController.performTask("MOVE");

        assertEquals(new Robot(FacingDirection.NORTH, new Position(0,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("LEFT");
        robotController.performTask("LEFT");

        for (int i = 0; i < 5; i++)
            robotController.performTask("MOVE");

        assertEquals(new Robot(FacingDirection.SOUTH, new Position(0,0)), robotController.getActionFactory().getRobot());
    }

    @Test
    void tryGoOutFromTheTable()
    {
        robotController.performTask("PLACE 0,0,WEST");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.WEST, new Position(0,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 0,0,SOUTH");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.SOUTH, new Position(0,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 0,5,NORTH");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.NORTH, new Position(0,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 0,5,WEST");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.WEST, new Position(0,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,0,EAST");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.EAST, new Position(5,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,0,SOUTH");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.SOUTH, new Position(5,0)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,5,NORTH");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.NORTH, new Position(5,5)), robotController.getActionFactory().getRobot());

        robotController.performTask("PLACE 5,5,EAST");
        robotController.performTask("MOVE");
        assertEquals(new Robot(FacingDirection.EAST, new Position(5,5)), robotController.getActionFactory().getRobot());

    }

}