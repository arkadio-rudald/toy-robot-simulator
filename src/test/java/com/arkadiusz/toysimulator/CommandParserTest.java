package com.arkadiusz.toysimulator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
/*
Unit tests of RobotController class
*/
class CommandParserTest {
    private CommandParser commandParser;


    @BeforeEach
    void initializeCommandParser()
    {
        commandParser = new CommandParser();
    }

    @Test
    void shouldReturnTrueAllCommandsAreCorrect() {
        assertEquals(true, commandParser.isInputCommandCorrect("MOVE"));
        assertEquals(true, commandParser.isInputCommandCorrect("LEFT"));
        assertEquals(true, commandParser.isInputCommandCorrect("RIGHT"));
        assertEquals(true, commandParser.isInputCommandCorrect("REPORT"));
    }

    @Test
    void shouldReturnFalseWrongCommand()
    {
        assertEquals(false, commandParser.isInputCommandCorrect("PLACE"));
        assertEquals(false, commandParser.isInputCommandCorrect("MOWE"));
        assertEquals(false, commandParser.isInputCommandCorrect("PLACE 0,0"));
        assertEquals(false, commandParser.isInputCommandCorrect("PLACE 0,SOUTH"));

    }

    @Test
    void shouldReturnTruePlacingEveryDirection()
    {
        assertEquals(true, commandParser.isInputCommandCorrect("PLACE 0,0,NORTH"));
        assertEquals(true, commandParser.isInputCommandCorrect("PLACE 0,0,SOUTH"));
        assertEquals(true, commandParser.isInputCommandCorrect("PLACE 0,0,EAST"));
        assertEquals(true, commandParser.isInputCommandCorrect("PLACE 0,0,WEST"));

    }
}