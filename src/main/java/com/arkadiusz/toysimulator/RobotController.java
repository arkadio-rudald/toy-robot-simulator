package com.arkadiusz.toysimulator;


/*
    Robot Controller analysis the input string
    and tells the robot what to do.
    This class has been created to connect Robot
    interface and User Interface
*/
public class RobotController {
    private ActionFactory actionFactory;

    RobotController(ActionFactory actionFactory) {
        this.actionFactory = actionFactory;
    }

    public ActionFactory getActionFactory() {
        return actionFactory;
    }

    public void performTask(String command)
    {
        if (new CommandParser().isInputCommandCorrect(command))
            actionFactory.doRobotAction(new CommandParser().parseCommandToEnum(command));
    }

    public void start() {
        String text = "";
        while (!text.equals(CommandParser.STOP)) {
            text = new CommandParser().readLine();
            performTask(text);
        }
    }
}
