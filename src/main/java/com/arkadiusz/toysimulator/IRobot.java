package com.arkadiusz.toysimulator;

public interface IRobot {

    void move();

    void turnRight();

    void turnLeft();

    String report();
}
