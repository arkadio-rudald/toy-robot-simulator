package com.arkadiusz.toysimulator.model;

public class Table {
    private int minHorizontal;
    private int minVertical;
    private int maxHorizontal;
    private int maxVertical;

    public Table(int minHorizontal, int minVertical, int maxHorizontal, int maxVertical) {
        this.minHorizontal = minHorizontal;
        this.minVertical = minVertical;
        this.maxHorizontal = maxHorizontal;
        this.maxVertical = maxVertical;
    }

    public boolean isPositionOnTheTable(Position position)
    {
        return position.getY() >= this.getMinVertical()
                && position.getY() <= this.getMaxVertical()
                && position.getX() >= this.getMinHorizontal()
                && position.getX() <= this.getMaxHorizontal();
    }

    public int getMinHorizontal() {
        return minHorizontal;
    }

    public int getMinVertical() {
        return minVertical;
    }

    public int getMaxHorizontal() {
        return maxHorizontal;
    }

    public int getMaxVertical() {
        return maxVertical;
    }
}
