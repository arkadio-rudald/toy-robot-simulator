package com.arkadiusz.toysimulator.model;

public enum Command {
    PLACE(0,0,0),
    MOVE,
    LEFT,
    RIGHT,
    REPORT;

    private int direction;
    private int x;
    private int y;

    Command() {
    }

    Command(int x, int y, int direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
