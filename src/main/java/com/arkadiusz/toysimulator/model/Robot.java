package com.arkadiusz.toysimulator.model;

import com.arkadiusz.toysimulator.IRobot;

import java.util.Arrays;
import java.util.Objects;

public class Robot implements IRobot {
    private FacingDirection facingDirection;
    private Position currentPosition;

    public Robot(FacingDirection facingDirection, Position currentPosition) {
        this.facingDirection = facingDirection;
        this.currentPosition = currentPosition;
    }



    @Override
    public void move() {
        switch (facingDirection)
        {
            case NORTH:
                currentPosition.setY(currentPosition.getY() + 1);
                break;
            case WEST:
                currentPosition.setX(currentPosition.getX() - 1);
                break;
            case SOUTH:
                currentPosition.setY(currentPosition.getY() - 1);
                break;
            case EAST:
                currentPosition.setX(currentPosition.getX() + 1);
                break;
        }
    }

    @Override
    public void turnRight() {
        int index = Arrays.asList(FacingDirection.values()).indexOf(facingDirection);

        if (index == 0)
            facingDirection = FacingDirection.values()[FacingDirection.values().length-1];
        else
            facingDirection = FacingDirection.values()[index-1];
    }

    @Override
    public void turnLeft() {
        int index = Arrays.asList(FacingDirection.values()).indexOf(facingDirection);

        if (index < 3)
            facingDirection = FacingDirection.values()[index+1];
        else
            facingDirection = FacingDirection.values()[0];
    }

    @Override
    public String report() {
        return currentPosition.getX() + "," + currentPosition.getY() + "," + facingDirection.name();
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public FacingDirection getFacingDirection() {
        return facingDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return facingDirection == robot.facingDirection &&
                Objects.equals(currentPosition, robot.currentPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(facingDirection, currentPosition);
    }
}
