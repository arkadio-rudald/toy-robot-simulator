package com.arkadiusz.toysimulator.model;

public enum FacingDirection {
    NORTH(0),
    WEST(1),
    SOUTH(2),
    EAST(3);

    private int value;

    FacingDirection(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static FacingDirection getEnumByValue(int value)
    {
        switch(value)
        {
            case 0:
                return NORTH;
            case 1:
                return WEST;
            case 2:
                return SOUTH;
            case 3:
                return EAST;
            default:
                return NORTH;
        }

    }
}
