package com.arkadiusz.toysimulator;

import com.arkadiusz.toysimulator.model.Command;
import com.arkadiusz.toysimulator.model.FacingDirection;
import com.arkadiusz.toysimulator.model.Position;
import com.arkadiusz.toysimulator.model.Robot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CommandParser {
    public static final String STOP = "STOP";


    public String readLine()
    {
        String inputString = null;
        try{
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            inputString = bufferRead.readLine();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        return inputString;
    }

    boolean isInputCommandCorrect(String command)
    {
        if (command.contains(Command.PLACE.toString()))
        {
            String [] placeCommandAsArray = parseCommandToArray(command);
            return (
                    placeCommandAsArray.length == 4
                            && placeCommandAsArray[0].equals(Command.PLACE.toString())
                            && placeCommandAsArray[1].matches("-?\\d")
                            && placeCommandAsArray[2].matches("-?\\d")
                            && !Arrays.stream(FacingDirection.values())
                            .filter(comm -> comm.toString().equals(placeCommandAsArray[3]))
                            .collect(Collectors.toList())
                            .isEmpty());
        }
        else {
            return (!Arrays.stream(Command.values())
                    .filter(comm -> comm.name().equals(command))
                    .collect(Collectors.toList())
                    .isEmpty());
        }
    }

    private String[] parseCommandToArray(String command)
    {
        return command.split("[\\s,]");
    }

    protected Command parseCommandToEnum(String comm) {
        if (comm.contains(Command.PLACE.toString())) {
            String[] placeCommandAsArray = comm.split("[\\s,]");

            int horizontalCoordinate = Integer.valueOf(placeCommandAsArray[1]);
            int verticalCoordinate = Integer.valueOf(placeCommandAsArray[2]);
            int facingDirection = Arrays.stream(FacingDirection.values())
                    .filter(direction -> direction.name().equals(placeCommandAsArray[3]))
                    .collect(Collectors.toList()).get(0).getValue();

            Command place = Command.PLACE;
            place.setX(horizontalCoordinate);
            place.setY(verticalCoordinate);
            place.setDirection(facingDirection);

            return place;
        }
        else
        {
            return Arrays.stream(Command.values())
                    .filter(com -> com.name().contains(comm))
                    .collect(Collectors.toList()).get(0);
        }
    }
}
