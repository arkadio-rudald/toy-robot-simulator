package com.arkadiusz.toysimulator;

import com.arkadiusz.toysimulator.model.Table;

public class ToyRobotSimulator {
    
    public static void main(String [] args)
    {
        RobotController robotController = new RobotController(
                new ActionFactory(
                        new Table(0,0,5,5)
                ));
        robotController.start();
    }
}
