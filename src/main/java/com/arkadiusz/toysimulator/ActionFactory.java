package com.arkadiusz.toysimulator;

import com.arkadiusz.toysimulator.model.*;

public class ActionFactory {
    private Robot robot;
    private Table table;

    ActionFactory(Table table) {
        this.table = table;
    }

    public void doRobotAction(Command command)
    {
        switch (command)
        {
            case PLACE:
                if (table.isPositionOnTheTable(new Position(command.getX(), command.getY())))
                    robot = new Robot(
                            FacingDirection.getEnumByValue(command.getDirection()),
                            new Position(command.getX(), command.getY())
                    );
                else
                    robot = null;
                break;
            case LEFT:
                robot.turnLeft();
                break;
            case RIGHT:
                robot.turnRight();
                break;
            case MOVE:
                if (!isRobotGoingToFall(robot,table))
                    robot.move();
                break;
            case REPORT:
                System.out.println(robot.report());
                break;
        }
    }

    private boolean isRobotGoingToFall(Robot robot, Table table) {
        return (robot.getCurrentPosition().getY() == table.getMinVertical() && robot.getFacingDirection() == FacingDirection.SOUTH)
                || (robot.getCurrentPosition().getY() == table.getMaxVertical() && robot.getFacingDirection() == FacingDirection.NORTH)
                || (robot.getCurrentPosition().getX() == table.getMinHorizontal() && robot.getFacingDirection() == FacingDirection.WEST)
                || (robot.getCurrentPosition().getX() == table.getMaxHorizontal() && robot.getFacingDirection() == FacingDirection.EAST);
    }
    public Robot getRobot() {
        return robot;
    }
}
